def read_avg_disp(input_file):
    with open(input_file, "r") as input_stream:
        str_buff = input_stream.readline()
        avg_str, disp_str = str_buff.split()
        avg = float(avg_str)
        disp = float(disp_str)
        return avg, disp 

def write_values(values_list, output_file):
    with open(output_file,"w") as output_stream:
        string_values_list = [str(item) for item in values_list]
        output_stream.write("\n".join(string_values_list))
