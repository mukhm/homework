import numpy as np

def generate_values(avg, disp, count):
    values_list = np.random.normal(avg, disp**0.5,count) 
    return values_list
